if not GuildWardenWin then GuildWardenWin = {} end
local index = 0
local toggleState=false
local procTable = {}
local classBool={true,true,true,true,true,true,true,true,true,true,true,true}
local sortmode=""
local offlineFilt=false
local partyFilt=false
GuildWarden.ToolTip={}

function GuildWardenWin.Toggle()

	if GameData.Guild.m_GuildID ~= 0 then
		toggleState= not toggleState		

		if toggleState then
			index=0
			GuildWarden.Labels()
		end

		WindowSetShowing("GuildWarden.DBWindow", toggleState)
	else
		EA_ChatWindow.Print(L"GuildWarden: You are not in a guild!")
		toggleState=false
		WindowSetShowing("GuildWarden.DBWindow", false)
	end
end

function GuildWardenWin.WinSetup()

	LayoutEditor.RegisterWindow( "GuildWarden.uiButton",
								L"GuildWarden.uiButton",
								L"GuildWarden Button",
								false, false,
								true, nil )

	ButtonSetText("GuildWarden.DBWindowBtn1", L"Filter")
	ButtonSetText("GuildWarden.DBWindowDec", L"+")
	ButtonSetText("GuildWarden.DBWindowInc", L"-")

	LabelSetText("GuildWarden.DBWindowNameF", L"Refine by Name")
	LabelSetText("GuildWarden.DBWindowLvlF", L"Lvl")
	LabelSetText("GuildWarden.DBWindowLvlD", L"-")
	LabelSetText("GuildWarden.DBWindowRrF", L"RR")
	LabelSetText("GuildWarden.DBWindowRrD", L"-")
	LabelSetText("GuildWarden.DBWindowHasF", L"Has")
	LabelSetText("GuildWarden.DBWindowNeedF", L"Needs")
	LabelSetText("GuildWarden.DBWindowWards", L"or more")
	LabelSetText("GuildWarden.DBWindowWTitle", L"--GuildWarden--")

	LabelSetText("GuildWarden.DBWindow.TextN0", L"Name")
	LabelSetText("GuildWarden.DBWindow.TextC0", L"Class")
	LabelSetText("GuildWarden.DBWindow.TextL0", L"Level")
	LabelSetText("GuildWarden.DBWindow.TextD0", L"Date")
	LabelSetText("GuildWarden.DBWindow.TextH0", L"")

	LabelSetText("GuildWarden.DBWindow.TextSigil", L"Wards")
	LabelSetText("GuildWarden.DBWindow.TextAnni", L"Anni")
	LabelSetText("GuildWarden.DBWindow.TextBL", L"BL")
	LabelSetText("GuildWarden.DBWindow.TextSenti", L"Sent")
	LabelSetText("GuildWarden.DBWindow.TextConq", L"Conq")
	LabelSetText("GuildWarden.DBWindow.TextDP", L"DP")
	LabelSetText("GuildWarden.DBWindow.TextInv", L"Inv")
	LabelSetText("GuildWarden.DBWindow.TextTyr", L"Tyrt")
	LabelSetText("GuildWarden.DBWindow.TextWar", L"War")

	LabelSetText("GuildWarden.DBWindowKey1", L"  Chest         Belt")
	LabelSetText("GuildWarden.DBWindowKey2", L" Gloves Shoulders")
	LabelSetText("GuildWarden.DBWindowKey3", L"  Helm         Boots")

	if offlineFilt then
		if partyFilt then
			LabelSetText("GuildWarden.DBWindowOffline", L"Showing Battlegroup")
		else
			LabelSetText("GuildWarden.DBWindowOffline", L"Showing Online")
		end
	else
		LabelSetText("GuildWarden.DBWindowOffline", L"Showing All")
	end

	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo", L"<no filter>")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo", L"Lesser Wards")			
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo", L"Greater Wards")			
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo", L"Superior Wards")

	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"<no filter>")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Lesser Wards")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Greater Wards")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Superior Wards")

if GameData.Player.realm==1 then
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Warpblade Tunnels")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Sigmar's Crypt")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Bastion Stair (for set)")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"WT(for set)")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"SC (for set)")
else
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Bloodwrought Enclave")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Bilerot Burrow")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Bastion Stair (for set)")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"BWE(for set)")
	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"BRB (for set)")
end

	ComboBoxAddMenuItem ("GuildWarden.DBWindowCombo2", L"Lost Vale (for set)")

	ComboBoxSetSelectedMenuItem ("GuildWarden.DBWindowCombo", 1)
	ComboBoxSetSelectedMenuItem ("GuildWarden.DBWindowCombo2", 1)

--	LabelSetText("GuildWarden.DBWindow.Text1", L"--GuildWarden--")
--	LabelSetText("GuildWarden.DBWindow.Text2", L"--GuildWarden--")
--	LabelSetText("GuildWarden.DBWindow.Text3", L"--GuildWarden--")
--	LabelSetText("GuildWarden.DBWindow.Text4", L"--GuildWarden--")
--	LabelSetText("GuildWarden.DBWindow.Text5", L"--GuildWarden--")

	local texture1, x1, y1 = GetIconData( 1 )
	DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon1", texture1, 5, 5 )
	DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon2", texture1, 5, 5 )
	DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon3", texture1, 5, 5 )
	DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon4", texture1, 5, 5 )
	DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon5", texture1, 5, 5 )

	TextEditBoxSetText("GuildWarden.DBWindowLvlRefU", towstring(GuildWarden.LvlU))
	TextEditBoxSetText("GuildWarden.DBWindowLvlRefL", towstring(GuildWarden.LvlL))
	TextEditBoxSetText("GuildWarden.DBWindowRrRefU", towstring(GuildWarden.RrU))
	TextEditBoxSetText("GuildWarden.DBWindowRrRefL", towstring(GuildWarden.RrL))
	TextEditBoxSetText("GuildWarden.DBWindowWardNo", towstring(GuildWarden.WardS))


	ComboBoxSetSelectedMenuItem ("GuildWarden.DBWindowCombo", tonumber(GuildWarden.Combo1))
	ComboBoxSetSelectedMenuItem ("GuildWarden.DBWindowCombo2", tonumber(GuildWarden.Combo2))


	for i=0,5 do
		for j=1,12 do
			k = j + 12*(2-GameData.Player.realm)
			ButtonSetTexture( "GuildWarden.DBWindow.ClassFilter"..j, i, GuildWarden.texture[k], GuildWarden.x[k], GuildWarden.y[k] )
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter"..j, 255,255,255)
		end
	end

end

function GuildWarden.InitTable()
	local m=1
	for key,data in pairs(GuildWarden.Data[GameData.Guild.m_GuildID]) do
		procTable[m] = data
		m=m+1
	end

	GuildWarden.Labels()
end

function GuildWarden.ScrollUp()
	index = index - 3
	if index < 0 then
		index =0
	end
	GuildWarden.Labels()
end
function GuildWarden.ScrollDown()
	if LabelGetText("GuildWarden.DBWindow.TextN5") ~= L"" then
		index = index + 3
		GuildWarden.Labels()
	end
end

function GuildWardenWin.Scroll(x,y,delta,flags)
	if delta>0 then
		GuildWarden.ScrollUp()
	elseif delta<0 then
		GuildWarden.ScrollDown()
	end
end

function GuildWarden.Labels()

	local key=nil
	local tableEnd=false
	local pos

	if index < 0 then
		index=0
	end

	for i=1,5 do
		local key = i+index
		text=procTable[key]
		if text == nil then
			tableEnd=true
		end

		text=towstring(text)
		text1=towstring(text)

		if key and not tableEnd then
			GuildWarden.dataCheck = false

			pos = text:find(L":")
			if pos ~= nil then
				local Pname = text:sub(1,pos-1)
				text = text:sub(pos + 1,-1)

				pos = text:find(L":")
				if pos ~= nil then
					local AData1 = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)
				pos = text:find(L":")
				if pos ~= nil then
					local AData2 = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)
				pos = text:find(L":")
				if pos ~= nil then
					local sigils = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)

						pos = text:find(L":")
						if pos ~= nil then
							local DateDay = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)
							pos = text:find(L":")
						if pos ~= nil then
							local DateMonth = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)

							pos = text:find(L":")
						if pos ~= nil then
							local DateYear = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)


							pos = text:find(L":")
							if pos ~= nil then
								local handedness = tonumber(text:sub(1,pos-1))
								text = text:sub(pos + 1,-1)
	
								pos = text:find(L":")
								if pos ~= nil then
									local ClassData = text:sub(1,pos-1)
									text = text:sub(pos + 1,-1)

									pos = text:find(L":")
									if pos ~= nil then
										local LevelData = text:sub(1,pos-1)
										text = text:sub(pos + 1,-1)
									pos = text:find(L":")

									local RRData
									local AData3
									if pos ~= nil then
										RRData = text:sub(1,pos-1)
										AData3 = text:sub(pos+1,-1)
									else
										RRData = text:sub(1,-1)
										AData3 = 0
									end

				GuildWarden.dataCheck=true
				LabelSetText("GuildWarden.DBWindow.TextN"..i, Pname)
				LabelSetText("GuildWarden.DBWindow.TextC"..i, ClassData)
				LabelSetText("GuildWarden.DBWindow.TextL"..i, LevelData..L"/"..RRData)
				LabelSetText("GuildWarden.DBWindow.TextD"..i, DateDay..L"/"..DateMonth..L"/"..DateYear)
				LabelSetText("GuildWarden.DBWindow.TextH"..i, towstring(handedness))

				armourchecks={GuildWarden.decompose(tonumber(AData1),tonumber(AData2),tonumber(AData3))}

			local lesser=0
			local greater=0
			local superior=0
			local excelsior=0
			local supreme=0

			if sigils:sub(1,1) ~=nil then
				if tonumber(sigils:sub(1,1)) ==1 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 100,100,255)
					lesser=lesser+1
				elseif tonumber(sigils:sub(1,1)) ==2 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 100,255,100)
					greater=greater+1
				elseif tonumber(sigils:sub(1,1)) ==3 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 192,192,0)
					superior=superior+1
				elseif tonumber(sigils:sub(1,1)) ==4 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 255,0,0)
					excelsior=excelsior+1
				elseif tonumber(sigils:sub(1,1)) ==5 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 192,0,192)
					supreme=supreme+1
				else
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 0,0,0	)
				end
			end

			if sigils:sub(2,2) ~=nil then
				if tonumber(sigils:sub(2,2)) ==1 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 100,100,255)
					lesser=lesser+1
				elseif tonumber(sigils:sub(2,2)) ==2 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 100,255,100)
					greater=greater+1
				elseif tonumber(sigils:sub(2,2)) ==3 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 192,192,0)
					superior=superior+1
				elseif tonumber(sigils:sub(2,2)) ==4 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 255,0,0)
					excelsior=excelsior+1
				elseif tonumber(sigils:sub(2,2)) ==5 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 192,0,192)
					supreme=supreme+1
				else
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 0,0,0	)
				end
			end

			if sigils:sub(3,3) ~=nil then
				if tonumber(sigils:sub(3,3)) ==1 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 100,100,255)
					lesser=lesser+1
				elseif tonumber(sigils:sub(3,3)) ==2 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 100,255,100)
					greater=greater+1
				elseif tonumber(sigils:sub(3,3)) ==3 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 192,192,0)
					superior=superior+1
				elseif tonumber(sigils:sub(3,3)) ==4 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 255,0,0)
					excelsior=excelsior+1
				elseif tonumber(sigils:sub(3,3)) ==5 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 192,0,192)
					supreme=supreme+1
				else
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 0,0,0	)
				end
			end

			if sigils:sub(4,4) ~=nil then
				if tonumber(sigils:sub(4,4)) ==1 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 100,100,255)
					lesser=lesser+1
				elseif tonumber(sigils:sub(4,4)) ==2 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 100,255,100)
					greater=greater+1
				elseif tonumber(sigils:sub(4,4)) ==3 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 192,192,0)
					superior=superior+1
				elseif tonumber(sigils:sub(4,4)) ==4 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 255,0,0)
					excelsior=excelsior+1
				elseif tonumber(sigils:sub(4,4)) ==5 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 192,0,192)
					supreme=supreme+1
				else
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 0,0,0	)
				end
			end

			if sigils:sub(5,5) ~=nil then
				if tonumber(sigils:sub(5,5)) ==1 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 100,100,255)
					lesser=lesser+1
				elseif tonumber(sigils:sub(5,5)) ==2 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 100,255,100)
					greater=greater+1
				elseif tonumber(sigils:sub(5,5)) ==3 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 192,192,0)
					superior=superior+1
				elseif tonumber(sigils:sub(5,5)) ==4 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 255,0,0)
					excelsior=excelsior+1
				elseif tonumber(sigils:sub(5,5)) ==5 then
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 192,0,192)
					supreme=supreme+1
				else
					WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 0,0,0	)
				end
			end

			GuildWarden.ToolTip[i]=L""
			if lesser ~= 0 then
				GuildWarden.ToolTip[i] = towstring(lesser)..L" Lesser Wards\n"
			end
			if greater ~= 0 then
				GuildWarden.ToolTip[i] = GuildWarden.ToolTip[i]..towstring(greater)..L" Greater Wards\n"
			end
			if superior ~= 0 then
				GuildWarden.ToolTip[i] = GuildWarden.ToolTip[i]..towstring(superior)..L" Superior Wards\n"
			end
			if excelsior ~= 0 then
				GuildWarden.ToolTip[i] = GuildWarden.ToolTip[i]..towstring(excelsior)..L" Excelsior Wards\n"
			end
			if supreme ~= 0 then
				GuildWarden.ToolTip[i] = GuildWarden.ToolTip[i]..towstring(supreme)..L" Supreme Wards\n"
			end

			if sigils == L"x" then
				GuildWarden.ToolTip[i] = L"Old version data!"
			end
			if sigils == L"00000" then
				GuildWarden.ToolTip[i] = L"(none)"
			end

			for j = 1,8 do
				local m=1
				if j<4 then m=5*(j-1) end
				if j>3 and j<8 then m=6*(j-3) + 10 end
				if j==8 then m=6*(j-3) + 11 end

				if armourchecks[1+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."A", "GWBoots", 0, 0 )
--					d("Boots")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."A", "GWBack5", 0, 0 )
				end

				if armourchecks[2+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."B", "GWGloves", 0, 0 )
--					d("Gloves")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."B", "GWBack5", 0, 0 )
				end
				if armourchecks[3+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."C", "GWShoulders", 0, 0 )
--					d("Shoudlers")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."C", "GWBack5", 0, 0 )
				end
				if armourchecks[4+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."D", "GWHelm", 0, 0 )
--					d("Helm")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."D", "GWBack5", 0, 0 )
				end
				if armourchecks[5+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."E", "GWChest", 0, 0 )
--					d("Chest")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."E", "GWBack5", 0, 0 )
				end
			end

			for j=1,6 do
				local m = 15 + (j-1)*6
				if j==6 then m=m+1 end

				if armourchecks[1+m] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.Belt"..j.."p"..i, "GWBelt", 0, 0 )
--					d("Belt")
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.Belt"..j.."p"..i, "GWBack", 0, 0 )
				end
			end

				if armourchecks[41] then
					DynamicImageSetTexture( "GuildWarden.DBWindow.Jewel1p"..i, "GWJewel", 0, 0 )
				else
					DynamicImageSetTexture( "GuildWarden.DBWindow.Jewel1p"..i, "GWBack", 0, 0 )
				end

				local texture1, x1, y1 = GetIconData( 1 )
				DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, texture1, 5, 5 )
				GuildWarden.setTexture(ClassData,i)


			end end end end end end end end end end 


		else	
			local texture1, x1, y1 = GetIconData( 1 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, texture1, 5, 5 )

			LabelSetText("GuildWarden.DBWindow.TextN"..i, L"")
			LabelSetText("GuildWarden.DBWindow.TextC"..i, L"")
			LabelSetText("GuildWarden.DBWindow.TextL"..i, L"")
			LabelSetText("GuildWarden.DBWindow.TextD"..i, L"")
			LabelSetText("GuildWarden.DBWindow.TextH"..i, L"")

			WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."A", 0,0,0	)
			WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."B", 0,0,0	)
			WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."C", 0,0,0	)
			WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."D", 0,0,0	)
			WindowSetTintColor("GuildWarden.DBWindow.sigilp"..i.."E", 0,0,0	)

			GuildWarden.ToolTip[i] = L""

			for j=1,8 do
			local texture1, x1, y1 = GetIconData( 1 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."A", texture1, 5, 5 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."B", texture1, 5, 5 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."C", texture1, 5, 5 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."D", texture1, 5, 5 )
			DynamicImageSetTexture( "GuildWarden.DBWindow.A"..j.."p"..i.."E", texture1, 5, 5 )
			end
			for j=1,6 do
			DynamicImageSetTexture( "GuildWarden.DBWindow.Belt"..j.."p"..i, texture1, 5, 5 )
			end
			DynamicImageSetTexture( "GuildWarden.DBWindow.Jewel1p"..i, texture1, 5, 5 )

			tableEnd=true		
		end
	end

end

function GuildWarden.setTexture(ClassData,i)
	--Dark Elves
	if ClassData:match(L"Disciple of Khaine") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[1], GuildWarden.x[1], GuildWarden.y[1] )
	end
	if ClassData:match(L"Blackguard") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[2], GuildWarden.x[2], GuildWarden.y[2] )
	end
	if ClassData:match(L"Witch Elf") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[3], GuildWarden.x[3], GuildWarden.y[3] )
	end
	if ClassData:match(L"Sorcerer") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[4], GuildWarden.x[4], GuildWarden.y[4] )
	end
	if ClassData:match(L"Sorceress") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[4], GuildWarden.x[4], GuildWarden.y[4] )
	end

	--Chaos
	if ClassData:match(L"Zealot") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[5], GuildWarden.x[5], GuildWarden.y[5] )
	end
	if ClassData:match(L"Chosen") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[6], GuildWarden.x[6], GuildWarden.y[6] )
	end
	if ClassData:match(L"Marauder") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[7], GuildWarden.x[7], GuildWarden.y[7] )
	end
	if ClassData:match(L"Magus") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[8], GuildWarden.x[8], GuildWarden.y[8] )
	end

	--Greenskins
	if ClassData:match(L"Shaman") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[9], GuildWarden.x[9], GuildWarden.y[9] )
	end
	if ClassData:match(L"Black Orc") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[10], GuildWarden.x[10], GuildWarden.y[10] )
	end
	if ClassData:match(L"Choppa") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[11], GuildWarden.x[11], GuildWarden.y[11] )
	end
	if ClassData:match(L"Squig Herder") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[12], GuildWarden.x[12], GuildWarden.y[12] )
	end

	--High Elves
	if ClassData:match(L"Archmage") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[13], GuildWarden.x[13], GuildWarden.y[13] )
	end
	if ClassData:match(L"Swordmaster") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[14], GuildWarden.x[14], GuildWarden.y[14] )
	end
	if ClassData:match(L"White Lion") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[15], GuildWarden.x[15], GuildWarden.y[15] )
	end
	if ClassData:match(L"Shadow Warrior") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[16], GuildWarden.x[16], GuildWarden.y[16] )
	end

	--Empire
	if ClassData:match(L"Warrior Priest") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[17], GuildWarden.x[17], GuildWarden.y[17] )
	end
	if ClassData:match(L"Knight of the Blazing Sun") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[18], GuildWarden.x[18], GuildWarden.y[18] )
	end
	if ClassData:match(L"Witch Hunter") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[19], GuildWarden.x[19], GuildWarden.y[19] )
	end
	if ClassData:match(L"Bright Wizard") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[20], GuildWarden.x[20], GuildWarden.y[20] )
	end

	--Dwarf
	if ClassData:match(L"Rune Priest") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[21], GuildWarden.x[21], GuildWarden.y[21] )
	end
	if ClassData:match(L"Ironbreaker") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[22], GuildWarden.x[22], GuildWarden.y[22] )
	end
	if ClassData:match(L"Slayer") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[23], GuildWarden.x[23], GuildWarden.y[23] )
	end
	if ClassData:match(L"Engineer") then
		DynamicImageSetTexture( "GuildWarden.DBWindow.ClassIcon"..i, GuildWarden.texture[24], GuildWarden.x[24], GuildWarden.y[24] )
	end
	return
end

function GuildWarden.filterReset()
	GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])

end

function GuildWarden.filter(filtertable)

	GuildWarden.LvlU = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowLvlRefU")))
	GuildWarden.LvlL = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowLvlRefL")))
	GuildWarden.RrU = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowRrRefU")))
	GuildWarden.RrL = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowRrRefL")))
	GuildWarden.WardS = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowWardNo")))

	GuildWarden.Combo1 = ComboBoxGetSelectedMenuItem("GuildWarden.DBWindowCombo")
	GuildWarden.Combo2 = ComboBoxGetSelectedMenuItem("GuildWarden.DBWindowCombo2")



	procTable=nil
	procTable={}
	local procTable1={}

	filtString = TextEditBoxGetText("GuildWarden.DBWindowNameRef")

	if  filtString ~= nil then
		for k,v in pairs(filtertable) do
			p=towstring(k)
			p=p:sub(1,filtString:len())
			p=p:lower()
			filtString = filtString:lower()
			if ( filtString==p ) then
				procTable1[k] = filtertable[k]
			end
		end
	else
		for k,v in pairs(filtertable) do
			procTable1[k]=v
		end
	end

	for key,text in pairs (procTable1) do

			pos = text:find(L":")
			if pos ~= nil then
				Pname = text:sub(1,pos-1)
				text = text:sub(pos + 1,-1)

				pos = text:find(L":")
				if pos ~= nil then
					AData1 = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)

				pos = text:find(L":")
				if pos ~= nil then
					AData2 = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)

				pos = text:find(L":")
				if pos ~= nil then
					sigils = text:sub(1,pos-1)
					text = text:sub(pos + 1,-1)

						pos = text:find(L":")
						if pos ~= nil then
							DateDay = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)

							pos = text:find(L":")
							if pos ~= nil then
							DateMonth = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)

							pos = text:find(L":")
							if pos ~= nil then
							DateYear = text:sub(1,pos-1)
							text = text:sub(pos + 1,-1)

							pos = text:find(L":")
							if pos ~= nil then
								handedness = tonumber(text:sub(1,pos-1))
								text = text:sub(pos + 1,-1)
	
								pos = text:find(L":")
								if pos ~= nil then
									ClassData = text:sub(1,pos-1)
									text = text:sub(pos + 1,-1)

									pos = text:find(L":")
									if pos ~= nil then
										LevelData = tonumber(text:sub(1,pos-1))
										text = text:sub(pos + 1,-1)
									pos = text:find(L":")
									if pos ~= nil then
										RRData = tonumber(text:sub(1,pos-1))
										text = text:sub(pos + 1,-1)
										 AData3 = text:sub(1,-1)
									else
										RRData = tonumber(text:sub(1,-1))
										AData3 = 0

			end end end end end end end end end end end

			lFilterU = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowLvlRefU")))
			if lFilterU == nil then
				lFilterU=40
			end
			lFilterL = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowLvlRefL")))
			if lFilterL == nil then
				lFilterL=1
			end

			if (LevelData < lFilterL) or (LevelData > lFilterU) then
				procTable1[key]=nil
			end

			lFilterU = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowRrRefU")))
			if lFilterU == nil then
				lFilterU=80
			end
			lFilterL = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowRrRefL")))
			if lFilterL == nil then
				lFilterL=0
			end

			if (RRData < lFilterL) or (RRData > lFilterU) then
				procTable1[key]=nil
			end

			if ((ClassData==L"Disciple of Khaine") or (ClassData==L"Archmage")) and not classBool[1] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Blackguard") or (ClassData==L"Swordmaster")) and not classBool[2] then
				procTable1[key]=nil
			end
			if ((ClassData==L"White Lion") or (ClassData==L"Witch Elf")) and not classBool[3] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Sorcerer") or (ClassData==L"Sorceress") or (ClassData==L"Shadow Warrior")) and not classBool[4] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Zealot") or (ClassData==L"Warrior Priest")) and not classBool[5] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Chosen") or (ClassData==L"Knight of the Blazing Sun")) and not classBool[6] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Witch Hunter") or (ClassData==L"Marauder")) and not classBool[7] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Magus") or (ClassData==L"Bright Wizard")) and not classBool[8] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Shaman") or (ClassData==L"Rune Priest")) and not classBool[9] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Black Orc") or (ClassData==L"Ironbreaker")) and not classBool[10] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Choppa") or (ClassData==L"Slayer")) and not classBool[11] then
				procTable1[key]=nil
			end
			if ((ClassData==L"Squig Herder") or (ClassData==L"Engineer")) and not classBool[12] then
				procTable1[key]=nil
			end

			if not GuildWarden.GetCombosFilters(tonumber(AData1), tonumber(AData2), tonumber(AData3), sigils) then
				procTable1[key]=nil
			end

	end

		local m=1

		if offlineFilt == true then
			if partyFilt == true then
				if IsWarBandActive() then
					for i = 1,4 do
						if GetBattlegroupMemberData()[i].players ~=nil then
							for k,v in ipairs (GetBattlegroupMemberData()[i].players) do
								if v.name~=nil and procTable1[WStringToString(v.name:sub(1,-1))]~=nil then
									procTable[m]=procTable1[WStringToString(v.name:sub(1,-1))]
									m=m+1
								end
							end
							if m==1 and i==4 then
								procTable[1]=procTable1[WStringToString(GameData.Player.name:sub(1,-3))]
								m=2
							end	
						else
							procTable[1]=procTable1[WStringToString(GameData.Player.name:sub(1,-3))]
							m=2
						end
					end
				else
						if GetGroupData() ~=nil then
							for k,v in ipairs (GetGroupData()) do
								if v.name~=nil and procTable1[WStringToString(v.name:sub(1,-1))]~=nil then
									procTable[m]=procTable1[WStringToString(v.name:sub(1,-1))]
									m=m+1
								end
							end
								procTable[m]=procTable1[WStringToString(GameData.Player.name:sub(1,-3))]
						else
							procTable[1]=procTable1[WStringToString(GameData.Player.name:sub(1,-3))]
							m=2
						end
				end
			else
				for k,v in ipairs (GetGuildMemberData()) do
					if procTable1[WStringToString(v.name:sub(1,-3))]~=nil and v.zoneID~=0 then
						procTable[m]=procTable1[WStringToString(v.name:sub(1,-3))]
						m=m+1
					end
				end
			end
		else
			for k,v in pairs(procTable1) do
				if v ~= nil then
					procTable[m]=v
					m=m+1
				end
			end
		end

	if sortmode=="name" or sortmode=="nameB" then
		table.sort(procTable, GuildWarden.nameSortD)
		GuildWarden.Labels()
	elseif sortmode=="level" or sortmode=="levelB" then
		table.sort(procTable, GuildWarden.levelSortD)
		GuildWarden.Labels()
	elseif sortmode=="date" or sortmode=="dateB" then
		table.sort(procTable, GuildWarden.dateSortD)
		GuildWarden.Labels()
	elseif sortmode=="class" or sortmode=="classB" then
		table.sort(procTable, GuildWarden.classSortD)
		GuildWarden.Labels()
	elseif sortmode=="sigil" or sortmode=="sigilB" then
		table.sort(procTable, GuildWarden.sigilSortD)
		GuildWarden.Labels()
	else
		GuildWarden.Labels()
	end


end

function GuildWardenWin.CF1(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[1] = not classBool[1]
		if classBool[1] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter1", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter1", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF2(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[2] = not classBool[2]
		if classBool[2] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter2", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter2", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF3(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[3] = not classBool[3]
		if classBool[3] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter3", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter3", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])	
	end
end
function GuildWardenWin.CF4(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[4] = not classBool[4]
		if classBool[4] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter4", 255,255,255)
			else
		WindowSetTintColor("GuildWarden.DBWindow.ClassFilter4", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF5(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[5] = not classBool[5]
		if classBool[5] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter5", 255,255,255)
		else
				WindowSetTintColor("GuildWarden.DBWindow.ClassFilter5", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF6(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[6] = not classBool[6]
		if classBool[6] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter6", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter6", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF7(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[7] = not classBool[7]
		if classBool[7] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter7", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter7", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF8(flags, x, y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[8] = not classBool[8]
		if classBool[8] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter8", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter8", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF9(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[9] = not classBool[9]
		if classBool[9] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter9", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter9", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF10(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[10] = not classBool[10]
		if classBool[10] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter10", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter10", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF11(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF8(0, x, y)
		GuildWardenWin.CF12(0, x, y)
	else
		classBool[11] = not classBool[11]
		if classBool[11] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter11", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter11", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end
function GuildWardenWin.CF12(flags,x,y)
	if flags == SystemData.ButtonFlags.SHIFT then
		GuildWardenWin.CF1(0, x, y)
		GuildWardenWin.CF2(0, x, y)
		GuildWardenWin.CF3(0, x, y)
		GuildWardenWin.CF4(0, x, y)
		GuildWardenWin.CF5(0, x, y)
		GuildWardenWin.CF6(0, x, y)
		GuildWardenWin.CF7(0, x, y)
		GuildWardenWin.CF9(0, x, y)
		GuildWardenWin.CF10(0, x, y)
		GuildWardenWin.CF11(0, x, y)
		GuildWardenWin.CF8(0, x, y)
	else
		classBool[12] = not classBool[12]
		if classBool[12] then
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter12", 255,255,255)
		else
			WindowSetTintColor("GuildWarden.DBWindow.ClassFilter12", 63,63,63)
		end
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end
end

function GuildWardenWin.Offline()
	if partyFilt then 
		offlineFilt = false
		partyFilt = false
	else
		if offlineFilt then
			partyFilt = true
		else
			offlineFilt = true
		end
	end

	if offlineFilt then
		if partyFilt then
			LabelSetText("GuildWarden.DBWindowOffline", L"Showing Battlegroup")
		else
			LabelSetText("GuildWarden.DBWindowOffline", L"Showing Online")
		end
	else
		LabelSetText("GuildWarden.DBWindowOffline", L"Showing All")
	end

	GuildWarden.filterReset()
end

function GuildWarden.decompose(number1, number2, number3)

	no1 = tonumber(number1)
	no2 = tonumber(number2)
	no3 = tonumber(number3)

	armour_flags={}
	for i=1,24 do
		if no2 >= (2^(24-i)) then
			armour_flags[35-i]=true
			no2 = no2 - 2^(24-i)

		else
			armour_flags[35-i]=false
		end
	end

	for i=1,10 do
		if no1 >= (2^(10-i)) then
			armour_flags[11-i]=true
			no1 = no1 - 2^(10-i)

		else
			armour_flags[11-i]=false
		end
	end

	for i=1,13 do
		if no3 >= (2^(13-i)) then
			armour_flags[48-i]=true
			no3 = no3 - 2^(13-i)

		else
			armour_flags[48-i]=false
		end
	end

--return flags in a more standard order (stored numerical order is that used in the WAR API, and is not consistant)
--new order is boots, gloves, shoulders, helm, chest, [belt], [jewelry]
--sets are ordered: Annihilator, Bloodlord, Sentinel, Dark Promise, Conqueror, Invader (this is the same as the WAR API)
	return armour_flags[1], armour_flags[2], armour_flags[4], armour_flags[3], armour_flags[5],
		 armour_flags[10],armour_flags[9], armour_flags[7], armour_flags[8], armour_flags[6], 
		 armour_flags[11],armour_flags[12],armour_flags[14],armour_flags[15],armour_flags[16], armour_flags[13],
		 armour_flags[20],armour_flags[21],armour_flags[18],armour_flags[19],armour_flags[17], armour_flags[22],
		 armour_flags[23],armour_flags[24],armour_flags[26],armour_flags[27],armour_flags[28], armour_flags[25],
		 armour_flags[29],armour_flags[30],armour_flags[32],armour_flags[33],armour_flags[34], armour_flags[31],
		 armour_flags[35],armour_flags[36],armour_flags[38],armour_flags[40],armour_flags[41], armour_flags[39],armour_flags[37],
		 armour_flags[45],armour_flags[46],armour_flags[43],armour_flags[44],armour_flags[42], armour_flags[47]
end

function GuildWarden.GetCombosFilters(number1, number2, number3, sigil)
	local lesser=0
	local greater=0
	local superior=0

	local armourchecks={}
	armourchecks = {GuildWarden.decompose(number1,number2,number3)}

	if sigil == L"x" then
		--feet
		if armourchecks[17] or armourchecks[29] then --dark promise or invader
			superior = superior+1
			greater = greater+1
			lesser = lesser+1
		else
			if armourchecks[11] or armourchecks[23] then --sentinel or conqueror
				greater = greater+1
				lesser = lesser+1
			else
				if armourchecks[1] or armourchecks[6] then --blood lord or annihilator
				lesser = lesser+1
				end
			end	
		end
		--hands
		if armourchecks[18] or armourchecks[30] then --dark promise or invader
			superior = superior+1
			greater = greater+1
			lesser = lesser+1
		else
			if armourchecks[12] or armourchecks[24] then --sentinel or conqueror
				greater = greater+1
				lesser = lesser+1
			else
				if armourchecks[2] or armourchecks[7] then --blood lord or annihilator
					lesser = lesser+1
				end
			end	
		end
		--shoulders
		if armourchecks[19] or armourchecks[31] then --dark promise or invader
			superior = superior+1
			greater = greater+1
			lesser = lesser+1
		else
			if armourchecks[13] or armourchecks[25] then --sentinel or conqueror
				greater = greater+1
				lesser = lesser+1
			else
				if armourchecks[3] or armourchecks[8] then --blood lord or annihilator
					lesser = lesser+1
				end
			end	
		end
		--head
		if armourchecks[20] or armourchecks[32] then --dark promise or invader
			superior = superior+1
			greater = greater+1
			lesser = lesser+1
		else
			if armourchecks[14] or armourchecks[26] then --sentinel or conqueror
				greater = greater+1
				lesser = lesser+1
			else
				if armourchecks[4] or armourchecks[9] then --blood lord or annihilator
					lesser = lesser+1
				end
			end	
		end
		--body
		if armourchecks[21] or armourchecks[33] then --dark promise or invader
			superior = superior+1
			greater = greater+1
			lesser = lesser+1
		else
			if armourchecks[15] or armourchecks[27] then --sentinel or conqueror
				greater = greater+1
				lesser = lesser+1
			else
				if armourchecks[5] or armourchecks[10] then --blood lord or annihilator
					lesser = lesser+1
				end
			end	
		end
	else
		local sigil_parts = {tonumber(sigil:sub(1,1)),tonumber(sigil:sub(2,2)),tonumber(sigil:sub(3,3)),tonumber(sigil:sub(4,4)),tonumber(sigil:sub(5,5))}
		for i=1,5 do
			if sigil_parts[i] > 0 then
				lesser=lesser+1
				if sigil_parts[i] > 1 then
					greater=greater+1
					if sigil_parts[i] > 2 then
						superior=superior+1
					end
				end
			end
		end
	end

	local boot=false	
	local helm=false
	local shoulder=false	
	local gloves=false	
	local chest=false	

	if armourchecks[11] or armourchecks[23] or armourchecks[17] or armourchecks[29]then
		boot = true
	end

	if armourchecks[14] or armourchecks[26] or armourchecks[20] or armourchecks[32] then
		helm  = true
	end

	if armourchecks[13] or armourchecks[19] or armourchecks[25] or armourchecks[31] then
		shoulder = true
	end

	if armourchecks[12] or armourchecks[18] or armourchecks[24] or armourchecks[30] then
		gloves  = true
	end

	if armourchecks[15] or armourchecks[21] or armourchecks[27] or armourchecks[33] then
		chest = true
	end


	local has = ComboBoxGetSelectedMenuItem("GuildWarden.DBWindowCombo")
	local needs = ComboBoxGetSelectedMenuItem("GuildWarden.DBWindowCombo2")
	local no_ward = tonumber(tostring(TextEditBoxGetText("GuildWarden.DBWindowWardNo")))

	if no_ward == nil then
		no_ward=0
	end

	if has == 2 and lesser < no_ward then
		return false
	end
	if has == 3 and greater < no_ward then
		return false
	end
	if has == 4 and superior < no_ward then
		return false
	end

	if needs == 2 and lesser ==5 then
		return false
	end
	if needs == 3 and greater ==5 then
		return false
	end
	if needs == 4 and superior ==5 then
		return false
	end


	if needs == 5 and boot and helm then
		return false
	end
	if needs == 6 and chest and shoulder and gloves then
		return false
	end
	
	if needs == 7 and armourchecks[6] and armourchecks[7] and armourchecks[8] and armourchecks[9] and armourchecks[10] then
		return false
	end

	if needs == 8 and armourchecks[11] and armourchecks[14] then
		return false
	end

	if needs == 9 and armourchecks[12] and armourchecks[13] and armourchecks[15] then
		return false
	end
	if needs == 10 and armourchecks[17] and armourchecks[18] and armourchecks[19] and armourchecks[20] and armourchecks[21] then
		return false
	end	

	return true
end

function GuildWardenWin.nameSort()
	index=0

	if sortmode == "name" then
		sortmode="nameB"
	else
		sortmode="name"
	end

	table.sort(procTable,GuildWarden.nameSortD)
	GuildWarden.Labels()
end

function GuildWarden.nameSortD(a,b)
	local pos

	if a and b then
		pos = a:find(L":")
		a= a:sub(1,pos-1)
		pos = b:find(L":")
		b= b:sub(1,pos-1)
	
		if sortmode=="name" then
			return a < b
		else
			return a > b
		end
	end
end


function GuildWardenWin.levelSort()
	index=0
	if sortmode =="level" then
		sortmode = "levelB"
	else
		sortmode="level"
	end

	table.sort(procTable, GuildWarden.levelSortD)
	GuildWarden.Labels()
end
function GuildWarden.levelSortD(a,b)
	local pos

	for i=1,9 do
		pos = a:find(L":")
		a = a:sub(pos+1,-1)
		pos = b:find(L":")
		b = b:sub(pos+1,-1)
	end

	pos = a:find(L":")
	local LvlA = tonumber(a:sub(1,pos-1))
	local RRA = tonumber(a:sub(pos+1,-1))
	pos = b:find(L":")
	local LvlB = tonumber(b:sub(1,pos-1))
	local RRB = tonumber(b:sub(pos+1,-1))

	if sortmode =="level" then
		if LvlA == LvlB then
			return RRA > RRB
		else
			return LvlA > LvlB
		end
	else
		if LvlA == LvlB then
			return RRA < RRB
		else
			return LvlA < LvlB
		end
	end
		
end

function GuildWardenWin.dateSort()
	index=0
	if sortmode =="date" then
		sortmode = "dateB"
	else
		sortmode="date"
	end

	table.sort(procTable, GuildWarden.dateSortD)
	GuildWarden.Labels()
end
function GuildWarden.dateSortD(a,b)
	local pos

	for i=1,4 do
		pos = a:find(L":")
		a = a:sub(pos+1,-1)
		pos = b:find(L":")
		b = b:sub(pos+1,-1)
	end

	pos = a:find(L":")
	local DayA = tonumber(a:sub(1,pos-1))
	a = a:sub(pos+1,-1)
	pos = a:find(L":")
	local MonA = tonumber(a:sub(1,pos-1))
	a = a:sub(pos+1,-1)
	pos = a:find(L":")
	local YearA = tonumber(a:sub(1,pos-1))	

	pos = b:find(L":")
	local DayB = tonumber(b:sub(1,pos-1))
	b = b:sub(pos+1,-1)
	pos = b:find(L":")
	local MonB = tonumber(b:sub(1,pos-1))
	b = b:sub(pos+1,-1)
	pos = b:find(L":")
	local YearB = tonumber(b:sub(1,pos-1))

	if sortmode=="date" then
		if YearA == YearB then
			if MonA == MonB then
				return DayA > DayB
			else
				return MonA > MonB
			end
		else
			return YearA > YearB
		end
	else
		if YearA == YearB then
			if MonA == MonB then
				return DayA < DayB
			else
				return MonA < MonB
			end
		else
			return YearA < YearB
		end

	end
end

function GuildWardenWin.classSort()
	index=0

	if sortmode=="class" then
		sortmode="classB"
	else
		sortmode="class"
	end

	table.sort(procTable, GuildWarden.classSortD)
	GuildWarden.Labels()
end
function GuildWarden.classSortD(a,b)
	local pos

	for i=1,8 do
		pos = a:find(L":")
		a = a:sub(pos+1,-1)
		pos = b:find(L":")
		b = b:sub(pos+1,-1)
	end

	pos = a:find(L":")
	local ClassA = a:sub(1,pos-1)
	pos = b:find(L":")
	local ClassB = b:sub(1,pos-1)

	if sortmode=="class" then
		return a < b
	else
		return a > b
	end
end
function GuildWardenWin.sigilSort()
	index=0

	if sortmode=="sigil" then
		sortmode="sigilB"
	else
		sortmode="sigil"
	end

	table.sort(procTable, GuildWarden.sigilSortD)
	GuildWarden.Labels()
end
function GuildWarden.sigilSortD(a,b)
	local pos

	local countA=0
	local countB=0

	for i=1,3 do
		pos = a:find(L":")
		a = a:sub(pos+1,-1)
		pos = b:find(L":")
		b = b:sub(pos+1,-1)
	end

	pos = a:find(L":")
	local ClassA = a:sub(1,pos-1)
	pos = b:find(L":")
	local ClassB = b:sub(1,pos-1)

	for i=1,5 do
		if ClassA:sub(i,-i)~=nil then
			countA = countA + (10 ^ tonumber(ClassA:sub(i,i)) )
		end
		if ClassB:sub(i,-i)~=nil then
			countB = countB + (10 ^ tonumber(ClassB:sub(i,i)) )
		end
	end

	if ClassA==L"x" then
		countA=0
	end
	if ClassB==L"x" then
		countB=0
	end

	if sortmode=="sigil" then
		return countA > countB
	else
		return countA < countB
	end
end

function GuildWarden.WardMouseOver1()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, GuildWarden.ToolTip[1])
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end
function GuildWarden.WardMouseOver2()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, GuildWarden.ToolTip[2])
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end
function GuildWarden.WardMouseOver3()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, GuildWarden.ToolTip[3])
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end
function GuildWarden.WardMouseOver4()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, GuildWarden.ToolTip[4])
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end
function GuildWarden.WardMouseOver5()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, GuildWarden.ToolTip[5])
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end