if not GuildWardenTTips then GuildWardenTTips = {} end

function GuildWardenTTips.sigil()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Wards")
    Tooltips.SetTooltipColor( 2, 1, 100,100,255)
    Tooltips.SetTooltipText (2, 1, L"Blue: Lesser")
	Tooltips.SetTooltipColor(3, 1, 100,255,100)
    Tooltips.SetTooltipText (3, 1, L"Green: Greater")
	Tooltips.SetTooltipColor( 4, 1,  192,192,0)
    Tooltips.SetTooltipText (4, 1, L"Yellow: Superior")
	Tooltips.SetTooltipColor( 5, 1, 255,0,0)
    Tooltips.SetTooltipText (5, 1, L"Red: Excelsior")
	Tooltips.SetTooltipColor( 6, 1, 192,0,192)
    Tooltips.SetTooltipText (6, 1, L"Purple: Supreme")

    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.anni()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Annihilator\n-from Tier 4 Keeps")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.bl()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Bloodlord\n-from Bastion Stair")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.sent()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Sentinel\n-from City Instances")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.conq()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Conqueror\n-from Fortresses")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.inv()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Invader\n-from City Invasion")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.dp()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Dark Promise\n-from Lost Vale")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.war()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Warlord\n-from Altdorf Pillage")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function GuildWardenTTips.tyr()
    local windowName = SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Tyrant\n-from Land of the Dead")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end