 if not GuildWarden then GuildWarden = {} end

	GuildWarden.Data = {}
	GuildWarden.Data[GameData.Guild.m_GuildID] = {}
	local setUp = false --only fill tables up once
	local FirstLoad --for checking if channel should be hidden
	local k=nil
	local newIn = true --for checking if you have finished initial sync
	local logged = false -- logged mid-downsync
	local AwaitSync = false -- waiting on someone who is down-syncing
	local delayedSync = false -- do you need to down-sync after you finish up-syncing
	local pinged = false
	local nominee =L""
	local last
	local lastBuffer =""
	local firstSync = true --for checking if you have started initial sync
	local x={}
	local y={}
	local texture={}
	local time=4
	local nextLine=false -- check that you have received the last packet you sent
	local timeDelParam = 4 -- INCREASE this parameter if you are having trouble with the chat throttle
	local timeDel = timeDelParam
	local timeLeft = timeDel +2
	local throttle = 0
	local G_ID=nil
	local channel=false
	local channelId
	local sendTable={}
	local reqTime = timeDel
	local jammed=false

function GuildWarden.Init_Warden()

	RegisterEventHandler(SystemData.Events.LOADING_END, "GuildWarden.EnterGame")	
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "GuildWarden.EnterGame")
	RegisterEventHandler(SystemData.Events.GUILD_REFRESH, "GuildWarden.GuildUpdate")
	RegisterEventHandler(SystemData.Events.QUIT, "GuildWarden.QuitSync")	
	RegisterEventHandler(SystemData.Events.LOG_OUT, "GuildWarden.QuitSync")
	RegisterEventHandler(SystemData.Events.TOME_OLD_WORLD_ARMORY_ARMOR_SET_UPDATED, "GuildWarden.ReSync")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "GuildWarden.ReSync")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "GuildWarden.ReSync")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_LEAVE, "GuildWarden.LeaveGuild")

	if GuildWarden.light == nil then
		GuildWarden.light=false
	end

	if not GuildWarden.light then
		RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "GuildWarden.OnChat")
	end

	CreateWindow("GuildWarden.uiButton", not GuildWarden.light)	
	CreateWindow("GuildWarden.DBWindow", false)

end

function GuildWarden.GuildUpdate()
	--entered guild (invite or on load in)
	if GameData.Guild.m_GuildID ~=0 and GameData.Guild.m_GuildID ~=nil and GameData.Guild.m_GuildName ~=L"" and GameData.Guild.m_GuildName ~=nil and G_ID==nil then
		G_ID = GameData.Guild.m_GuildID
		GuildWarden.ChanString = L"GWarden1p3"..GameData.Guild.m_GuildName..towstring(GameData.Guild.m_GuildID)

		if not GuildWarden.Data[GameData.Guild.m_GuildID] then
			GuildWarden.Data[GameData.Guild.m_GuildID] = {}
		end

		GuildWarden.InitTable()
		GuildWarden.RegStart()

	end
end

function GuildWarden.LeaveGuild()
	--left guild
	if G_ID~=nil and G_ID ~= 0 then
		GuildWarden.Purge(G_ID)
		toggleState = false
		WindowSetShowing("GuildWarden.DBWindow", toggleState)
		SystemData.UserInput.ChatText = L"/channelleave " ..GuildWarden.ChanString
		BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )

		G_ID = nil
		GuildWarden.ChanString = nil

		FirstLoad=true
		k=nil
		newIn = true
		logged = false
		AwaitSync = false
		delayedSync = false
		pinged = false
		nominee =L""
		last =""
		lastBuffer=""
		firstSync = true
		x={}
		y={}
		texture={}
		time=2
		nextLine=false
		timeDel = 2 -- INCREASE this parameter if you are having trouble with the chat throttle
		timeLeft = timeDel + 2
		throttle = 0

	end
end

function GuildWarden.RegStart()
	if newIn and firstSync then
		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.DelayStart")	
	end
end

function GuildWarden.DelayStart(elapsed)

	time=time - elapsed
	if time < 0 then
		time = 2
		if firstSync and GuildWarden.Data[GameData.Guild.m_GuildID] and GameData.Guild.m_GuildID~=0 and setUp then
			firstSync = false
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.DelayStart")

			channel, channelId = GuildWarden.FindChannel()	
	
			if not channel then
				k=nil
				newIn = true
				firstSync = true
				AwaitSync = false
				delayedSync = false
				pinged = false
	 			logged = false
				SystemData.UserInput.ChatText = L"/channeljoin " ..GuildWarden.ChanString
				BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
				GuildWarden.Sync()
	
			else
				if channel and not logged then	
					logged = false
					GuildWarden.Sync()
				end
			end

		end
	end
end


function GuildWarden.EnterGame()

	if not setUp then
		setUp = true
		GuildWarden.SetTexts()
		GuildWardenWin.WinSetup()
	end
	GuildWarden.GuildUpdate()

end


function GuildWarden.QuitSync()
	--warn others that you have logged out if you were in the middle of syncing someone, stop responding to requests
	logged =true

	if AwaitSync and nominee == GameData.Player.name and G_ID ~=0 then
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")	
		last = "syncQuit"
		GuildWarden.Sendtext()
	end
end

function GuildWarden.FindChannel()
    for k, v in pairs(ChatSettings.Channels) do
        if ( v.labelText ~= nil ) then
            if ( v.labelText:match(GuildWarden.ChanString) ) or ( v.labelText:match(wstring.lower(GuildWarden.ChanString) ) ) then
				return v.serverCmd ,k
            end
        end
    end
    
    return false, false
end

function GuildWarden.Sync()

	if newIn and G_ID~=0 then
		local ArmorData1=0
		local ArmorData2=0
		local ArmorData3=0

		--get armory data
		--anihilator
		if TomeGetOldWorldArmoryArmorSet(4) ~= nil then
			for j=1,5 do
				if TomeGetOldWorldArmoryArmorSet(4).pieces[j].unlocked then
					ArmorData1 = ArmorData1 + 2^(j-1)
--					EA_ChatWindow.Print(towstring(TomeGetOldWorldArmoryArmorSet(4).pieces[j].itemData.name))
				end
			end
		end
		
		--and bloodlord
 		if TomeGetOldWorldArmoryArmorSet(13) ~= nil then
			for j=1,5 do
				if TomeGetOldWorldArmoryArmorSet(13).pieces[j].unlocked then
					ArmorData1 = ArmorData1 + 2^(j+4)
				end
			end
		end

		--sent, conq, DP, Invader
		for i=1,4 do
			if TomeGetOldWorldArmoryArmorSet(i+13) ~= nil then
				for j=1,6 do
					if TomeGetOldWorldArmoryArmorSet(i+13).pieces[j].unlocked then
						ArmorData2 = ArmorData2 + 2^(6*i+j-7)
					end
				end
			end
		end

 		if TomeGetOldWorldArmoryArmorSet(18) ~= nil then
			for j=1,7 do
				if TomeGetOldWorldArmoryArmorSet(18).pieces[j].unlocked then
					ArmorData3 = ArmorData3 + 2^(j-1)
				end
			end
		end

 		if TomeGetOldWorldArmoryArmorSet(20) ~= nil then
			for j=1,6 do
				if TomeGetOldWorldArmoryArmorSet(20).pieces[j].unlocked then
					ArmorData3 = ArmorData3 + 2^(j+6)
				end
			end
		end


		local todaysDate = GetTodaysDate()
		local uptime = todaysDate.todaysDay..":"..todaysDate.todaysMonth..":"..todaysDate.todaysYear
	
		--update self
		local nom = WStringToString(GameData.Player.career.name:sub(1,-3))
		local Pname = WStringToString(GameData.Player.name:sub(1,-3))
		local sigils= GuildWarden.GetSigils()
		local dataP=Pname..":"..ArmorData1..":"..ArmorData2..":"..sigils..":"..uptime..":0:"..nom..":"..GameData.Player.level..":"..GameData.Player.Renown.curRank..":"..ArmorData3
		GuildWarden.Data[GameData.Guild.m_GuildID][Pname]=towstring(dataP)

		GuildWarden.filterReset()

		local channel, channelId = GuildWarden.FindChannel()
	
		FirstLoad = true
	
		if not channel then
			SystemData.UserInput.ChatText = L"/channeljoin " ..GuildWarden.ChanString
			BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
		end
	
		EA_ChatWindow.Print(towstring("GuildWarden: Starting upload."))
		k=nil	
		lastBuffer=""
--reg here		GuildWarden.sendNextDataBase("up")
		nextLine=true

		sendTable=nil
		sendTable={}
		local m=1
		local TempTable={}

		TempTable = GuildWarden.Data[G_ID]
		GuildWarden.Data=nil
		GuildWarden.Data={}
		GuildWarden.Data[G_ID]={}

		for k,v in ipairs (GetGuildMemberData()) do
			if TempTable[WStringToString(v.name:sub(1,-3))] ~=nil then
				GuildWarden.Data[G_ID][WStringToString(v.name:sub(1,-3))] = TempTable[WStringToString(v.name:sub(1,-3))]
			end			
			if GuildWarden.Data[G_ID][WStringToString(v.name:sub(1,-3))]~=nil and v.zoneID==0 then
				sendTable[m]=GuildWarden.Data[G_ID][WStringToString(v.name:sub(1,-3))]
				m=m+1
			end
		end
		
		TempTable=nil
--		sendTable[m]=GuildWarden.Data[G_ID][Pname]

		if (GuildWarden.lastSendDate==nil or GuildWarden.lastSendDate ~= uptime) and not GuildWarden.light then
			GuildWarden.lastSendDate = uptime
			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")
		else
			EA_ChatWindow.Print(towstring("GuildWarden: Finished upload."))
			newIn = false
			GuildWarden.lastSendDate = uptime
			reqTime=timeDel
			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Request")
		end
	end
end

function GuildWarden.Request(elapsed)
	reqTime = reqTime - elapsed
	if reqTime < 0 then
		last = "request"
		GuildWarden.Sendtext()
		reqTime=timeDel
	end	
end

function GuildWarden.ReSync()

if newIn then
	--GuildWarden.Sync()
elseif G_ID~=0 and GameData.Player.level~=0 and GameData.Player.Renown.curRank~=0 then

	local ArmorData1=0
	local ArmorData2=0
	local ArmorData3=0

	--get armory data
	--anihilator
	for j=1,5 do
		if TomeGetOldWorldArmoryArmorSet(4) ~= nil then
			if TomeGetOldWorldArmoryArmorSet(4).pieces[j].unlocked then
				ArmorData1 = ArmorData1 + 2^(j-1)
			end
		end
	end

	--and bloodlord
	for j=1,5 do
		if TomeGetOldWorldArmoryArmorSet(13) ~= nil then
			if TomeGetOldWorldArmoryArmorSet(13).pieces[j].unlocked then
				ArmorData1 = ArmorData1 + 2^(j+4)
			end
		end
	end

	--sent, conq, DP, Invader
	for i=1,4 do
		for j=1,6 do
			if TomeGetOldWorldArmoryArmorSet(i+13) ~= nil then
				if TomeGetOldWorldArmoryArmorSet(i+13).pieces[j].unlocked then
					ArmorData2 = ArmorData2 + 2^(6*i+j-7)
				end
			end
		end
	end

 		if TomeGetOldWorldArmoryArmorSet(18) ~= nil then
			for j=1,7 do
				if TomeGetOldWorldArmoryArmorSet(18).pieces[j].unlocked then
					ArmorData3 = ArmorData3 + 2^(j-1)
				end
			end
		end

 		if TomeGetOldWorldArmoryArmorSet(20) ~= nil then
			for j=1,6 do
				if TomeGetOldWorldArmoryArmorSet(20).pieces[j].unlocked then
					ArmorData3 = ArmorData3 + 2^(j+6)
				end
			end
		end

	local todaysDate = GetTodaysDate()
	local uptime = todaysDate.todaysDay..":"..todaysDate.todaysMonth..":"..todaysDate.todaysYear

	--update self
	local nom = WStringToString(GameData.Player.career.name:sub(1,-3))
	local Pname = WStringToString(GameData.Player.name:sub(1,-3))
	local sigils= GuildWarden.GetSigils()
	local dataP=towstring(Pname..":"..ArmorData1..":"..ArmorData2..":"..sigils..":"..uptime..":0:"..nom..":"..GameData.Player.level..":"..GameData.Player.Renown.curRank..":"..ArmorData3)

	local pos = GuildWarden.Data[GameData.Guild.m_GuildID][Pname]:find(L":")
	local text = GuildWarden.Data[GameData.Guild.m_GuildID][Pname]:sub(pos+1,-1)
	pos = text:find(L":")
	local AL1 = tonumber(text:sub(1,pos-1))
	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	local AL2 = tonumber(text:sub(1,pos-1))
	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	local Asigils = tonumber(text:sub(1,pos-1))

	text = text:sub(pos+1,-1)
	pos = text:find(L":")

	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	text = text:sub(pos+1,-1)
	pos = text:find(L":")

	text = text:sub(pos+1,-1)
	pos = text:find(L":")
	local Arank = tonumber(text:sub(1,pos-1))
	text = text:sub(pos+1,-1)
	pos = text:find(L":")

	local ARrank = tonumber(text:sub(1,pos-1))
	local AL3 = tonumber(text:sub(pos+1,-1))
-- EA_ChatWindow.Print(towstring(booltostring(pinged).." "..Pname.." ")..dataP)

	if ((GuildWarden.Data[GameData.Guild.m_GuildID][Pname] ~= dataP) or (pinged)) and (not (AL1 > ArmorData1)) and  (not (AL2 > ArmorData2)) and  (not (AL3 > ArmorData3)) and (not (Asigils>tonumber(sigils))
		and (not (Arank > GameData.Player.level)) and (not (ARrank > GameData.Player.Renown.curRank)) ) then

--		EA_ChatWindow.Print(L"RESYNC! "..dataP)
--		EA_ChatWindow.Print(L"RESYNC! "..GuildWarden.Data[GameData.Guild.m_GuildID][Pname])
--		EA_ChatWindow.Print(L"RESYNC! "..towstring(booltostring(pinged)))

		pinged = false
		GuildWarden.Data[GameData.Guild.m_GuildID][Pname]=dataP
		--send update
		last = "syncSend:"..WStringToString(dataP)
		GuildWarden.Sendtext()
	end
end
end

function GuildWarden.CheckData(txt)
	if G_ID ~=0 and G_ID~=nil then
	local accept = false
	local pos
	local AData3

	--check if data is better
			text = towstring(txt)
			pos = text:find(L":")
		if pos ~= nil then
				--remove sendSync
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				Pname = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local AData1 = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local AData2 = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local sigils = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local DateDay = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local DateMonth = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local DateYear = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
				local handedness = tonumber(text:sub(1,pos-1)) + 1

				text=text:sub(pos+1,-1)
				local DATE = towstring(DateDay..L":"..DateMonth..L":"..DateYear)
				wholeData = Pname..L":"..AData1..L":"..AData2..L":"..sigils..L":"..DATE..L":"..handedness..L":"..text

				pos = text:find(L":")

		if pos ~=nil then
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
		if pos ~=nil then
			local ARank = tonumber(text:sub(1,pos-1))
			local ARRank = text:sub(pos+1,-1)

			pos = ARRank:find(L":")
			if pos ~=nil then
				AData3 = tonumber(ARRank:sub(pos+1,-1))
				ARRank = tonumber(ARRank:sub(1,pos-1))
			else
				ARRank = tonumber(ARRank)
				AData3 = 0
				pos=1	
			end
		
--			EA_ChatWindow.Print(wholeData)

	text = GuildWarden.Data[GameData.Guild.m_GuildID][WStringToString(Pname)]

	if text == nil then
		 accept = true
	else

			text = text:sub(1,-1)
			pos = text:find(L":")
			text = text:sub(pos+1,-1)
			pos = text:find(L":")
				local EAData1 = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local EAData2 = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local Esigils = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local EDateDay = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local EDateMonth = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local EDateYear = text:sub(1,pos-1)
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local Ehandedness = tonumber(text:sub(1,pos-1))
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
			text = text:sub(pos + 1,-1)
			pos = text:find(L":")
				local ERank = tonumber(text:sub(1,pos-1))
				local ERRank = text:sub(pos+1,-1)

			pos = ERRank:find(L":")
			if pos ~=nil then
				EAData3 = tonumber(ERRank:sub(1+pos,-1))
				ERRank = tonumber(ERRank:sub(1,pos-1))	
			else
				ERRank = tonumber(ERRank)
				EAData3=0
				pos=1
			end

	if handedness == 1 then
	accept = true
	else
		if (tonumber(sigils) > tonumber(Esigils)) then
			accept = true
		elseif (tonumber(sigils) == tonumber(Esigils)) then
		if ( tonumber(AData1) > tonumber(EAData1) or tonumber(AData2) > tonumber(EAData2) or tonumber(AData3) > tonumber(EAData3) ) and ( not (tonumber(AData1) < tonumber(EAData1) or tonumber(AData2) < tonumber(EAData2) )) then
			accept = true
		elseif tonumber(AData1) == tonumber(EAData1) and tonumber(AData2) == tonumber(EAData2) and tonumber(AData3) == tonumber(EAData3) then
			if ARank > ERank or ARRank > ERRank then
				accept = true
			elseif ARank == ERank and ARRank == ERRank then
				if tonumber(DateYear) > tonumber(EDateYear) then
					accept = true
				elseif tonumber(DateYear) == tonumber(EDateYear) then
					if tonumber(DateMonth) > tonumber(EDateMonth) then
						accept = true
					elseif tonumber(DateMonth) == tonumber(EDateMonth) then
						if tonumber(DateDay) > tonumber(EDateDay) and DateDay ~= L".." then
							accept = true
						elseif tonumber(DateDay) == tonumber(EDateDay) then
							if handedness < Ehandedness then	
								accept = true
							end
						end
					end
				end
			end
		end
		end
	end
	end

		end end end end end end end end end end end--match to pos checks

	dash = txt:find(L"-")

	reject = false
	if dash then
		EA_ChatWindow.Print(L"GuildWarden: Bad Data Rejected")
		reject = true
	end

	--if so, save
		myName = GameData.Player.name:sub(1,-3)
--	EA_ChatWindow.Print(myName..L" "..Pname)
	if Pname~=myName then
	if (accept) and (pos ~= nil) and (not reject) and (Pname ~=nil) then
		GuildWarden.Data[GameData.Guild.m_GuildID][WStringToString(Pname)] = wholeData
		GuildWarden.filter(GuildWarden.Data[GameData.Guild.m_GuildID])
	end end

	end
	return
end

function GuildWarden.sendNextDataBase(dir)

if G_ID ~= 0 and G_ID~=nil then

	local key,datum =  next(sendTable,k)
--	local key,datum =  next(GuildWarden.Data[1],k)

	k=key

	if key==nil then
		if dir == "up" then
			last = "upDone"
			k=nil
		else
			last = "downDone"
			k=nil
			EA_ChatWindow.Print(L"GuildWarden: You have successfully updated other guild members!")
		end

	else
		last = "syncSend:"..WStringToString(towstring(datum))
	end

	lastBuffer=last

	GuildWarden.Sendtext()

end
end

function GuildWarden.Sending(elapsed)
	
	timeLeft = timeLeft - elapsed

	if timeLeft < 0 then

		if timeDel==timeDelParam or timeDel==(timeDelParam+1) then
			timeDel=timeDel+1
		else
			timeDel=timeDelParam
		end

		timeLeft = timeDel --reset timer

		if newIn then
			if nextLine==false then
				throttle = throttle + 15
				timeLeft = timeLeft + throttle
				if jammed then
					last=lastBuffer
					GuildWarden.Sendtext()
				else
					jammed=true
				end
			else
				jammed=false
				if throttle > 40 then
					throttle = 10
				elseif throttle > 0 then
					throttle = throttle -3
				end
				if throttle < 0 then
					throttle =0
				end
				timeLeft = timeLeft + throttle
				nextLine=false
				GuildWarden.sendNextDataBase("up")
			end
		else
			if nextLine==false then
				throttle = throttle + 45
				timeLeft = timeLeft + throttle
				if jammed then
					last=lastBuffer
					GuildWarden.Sendtext()
				else
					jammed=true
				end
			else
				jammed=false
				if throttle > 40 then
					throttle = 10
				elseif throttle > 0 then
					throttle = throttle -3
				end
				if throttle < 0 then
					throttle =0
				end
				timeLeft = timeLeft + throttle
				nextLine=false
				GuildWarden.sendNextDataBase("down")
			end
		end
	end
end

function GuildWarden.OnChat()
	if G_ID ~=0  and G_ID~=nil then

	local channel, channelId = GuildWarden.FindChannel()
	if channel and FirstLoad then
		GuildWarden.HideChannel(channelId)
		FirstLoad = false
	end

	if channel and not logged then
		local text = GameData.ChatData.text

		if AwaitSync and ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"syncQuit") and (GameData.Player.name ~= GameData.ChatData.name) ) then
		-- we were waiting on a sync, and the nominee left, rerequest sync
			if newIn then
				delayedSync= true
			else
				last = "nominate"
				GuildWarden.Sendtext()
				AwaitSync = false
				nominee =L""
			end
			return
		end

		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"upDone") and (GameData.Player.name ~= GameData.ChatData.name) ) then
			if not GuildWarden.light then
				if newIn then
					delayedSync = true
				else
					UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")	
					AwaitSync = false
					nominee =L""
					last = "nominate"
					GuildWarden.Sendtext()
				end
			end
			return
		end

-- turn off upload

		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"upDone") and (GameData.Player.name == GameData.ChatData.name) ) then
			EA_ChatWindow.Print(towstring("GuildWarden: Finished upload."))
			newIn = false
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")

			if not newIn and delayedSync then
				last = "nominate"
				GuildWarden.Sendtext()
			end

			return
		end
-- turn off download
		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"downDone") and (GameData.Player.name == GameData.ChatData.name) ) then
			delayedSync=false
			AwaitSync = false
			nominee =L""
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")
			return
		end


		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"downDone") ) then
			delayedSync=false
			AwaitSync = false
			nominee =L""
			return
		end

		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"request") ) then
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Request")
			pinged = true
			GuildWarden.ReSync()
		end

		if ( GameData.ChatData.type == channelId ) and ( GameData.ChatData.text:match(L"nominate") ) then
			delayedSync=false
			if not AwaitSync then
				nominee = GameData.ChatData.name
				AwaitSync = true
				if nominee:match(GameData.Player.name) then
					d("Nominated")
--reghere					GuildWarden.sendNextDataBase("down")
					nextLine=true

					sendTable=nil
					sendTable={}
					local m=1
					for k,v in ipairs (GetGuildMemberData()) do
						if GuildWarden.Data[G_ID][WStringToString(v.name:sub(1,-3))]~=nil and v.zoneID==0 then
							sendTable[m]=GuildWarden.Data[G_ID][WStringToString(v.name:sub(1,-3))]
							m=m+1
						end
						sendTable[m]=GuildWarden.Data[G_ID][WStringToString(GameData.Player.name:sub(1,-3))]							
					end
					RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "GuildWarden.Sending")	
				else
					--for when you are not the nominee
					pinged = true
					GuildWarden.ReSync()
				end
			end
			return
		end


		if ( GameData.ChatData.type == channelId ) and (GameData.Player.name ~= GameData.ChatData.name) then

			local pos = text:find(L":")
			if pos then
				pos=pos-1
				local id = text:sub(1,pos)
				--EA_ChatWindow.Print(id)
				if (id:match(L"syncSend")) then
					GuildWarden.CheckData(text)
				end
			end
		end

		if ( GameData.ChatData.type ~= channelId ) and (GameData.Player.name == GameData.ChatData.name) then
			if newIn or (nominee:match(GameData.Player.name) and AwaitSync) then
				timeLeft=timeLeft+4
			end
		end

		if ( GameData.ChatData.type == channelId ) and (GameData.Player.name == GameData.ChatData.name) then


			local pos = text:find(L":")
			if pos then
				pos=pos-1
				local id = text:sub(1,pos)
				if (id:match(L"syncSendS")) then
					EA_ChatWindow.Print(id)
					GuildWarden.CheckData(text)
					return
				end
			end


			if newIn then
				if ( GameData.ChatData.text:match(towstring(lastBuffer)) ) then
					nextLine=true
					return
				else
					return
				end
			end
			if nominee:match(GameData.Player.name) and AwaitSync then
				if GameData.ChatData.text:match(towstring(lastBuffer)) then
					nextLine=true
					return
				else
					return
				end
			end

		end 


	end

end
end

function GuildWarden.Sendtext()
	sendtext=towstring(last)
	local channel, channelId = GuildWarden.FindChannel()
		if not channel then
			SystemData.UserInput.ChatText = L"/channeljoin " ..GuildWarden.ChanString
			BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
		end

	if channel then
		SystemData.UserInput.ChatText = channel .. L" " .. towstring(sendtext)
		BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
	end

end

function GuildWarden.HideChannel(channelId)

	for _, wndGroup in ipairs(EA_ChatWindowGroups) do 
		if wndGroup.used == true then
			for tabId, tab in ipairs(wndGroup.Tabs) do
				local tabName = EA_ChatTabManager.GetTabName( tab.tabManagerId )
		
				if tabName then
					if tab.tabText ~= L""..GuildWarden.ChanString then
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, false)
					else
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, true)
						LogDisplaySetFilterColor(tabName.."TextLog", "Chat", channelId, 168, 187, 160 )
					end
				end
				
			end
			
		end
		
	end	
end

function GuildWarden.Purge(db)
	EA_ChatWindow.Print(L"GuildWarden: Expunging Database")
	GuildWarden.Data[db]=nil
--	GuildWarden.Data[db]={}
--	GuildWarden.ReSync()
	return
end

function GuildWarden.GetSigils()
	local sigil={}

	for i=1,5 do
		if TomeGetSigilEntry(3).fragments[i].isUnlocked then
			if TomeGetSigilEntry(4).fragments[i].isUnlocked then
				if TomeGetSigilEntry(5).fragments[i].isUnlocked then
					sigil[i]="5"
				else
					sigil[i]="4"
				end
			else
				sigil[i]="3"
			end
		else
			if TomeGetSigilEntry(2).fragments[i].isUnlocked then
				sigil[i]="2"
			else
				if TomeGetSigilEntry(1).fragments[i].isUnlocked then
					sigil[i]="1"
				else
					sigil[i]="0"
				end
			end
		end
	end
	
	local str = sigil[1]..sigil[2]..sigil[3]..sigil[4]..sigil[5]
	return str

end

