function GuildWarden.SetTexts()
	GuildWarden.x={}
	GuildWarden.y={}
	GuildWarden.texture={}

	GuildWarden.texture[1], GuildWarden.x[1], GuildWarden.y[1] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.BLOOD_PRIEST ) )
	GuildWarden.texture[2], GuildWarden.x[2], GuildWarden.y[2] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SHADE ) )
	GuildWarden.texture[3], GuildWarden.x[3], GuildWarden.y[3] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.ASSASSIN ) )
	GuildWarden.texture[4], GuildWarden.x[4], GuildWarden.y[4] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SORCERER ) )

	GuildWarden.texture[5], GuildWarden.x[5], GuildWarden.y[5] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.ZEALOT ) )
	GuildWarden.texture[6], GuildWarden.x[6], GuildWarden.y[6] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.CHOSEN ) )
	GuildWarden.texture[7], GuildWarden.x[7], GuildWarden.y[7] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.WARRIOR ) )
	GuildWarden.texture[8], GuildWarden.x[8], GuildWarden.y[8] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.MAGUS ) )

	GuildWarden.texture[9], GuildWarden.x[9], GuildWarden.y[9] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SHAMAN ) )
	GuildWarden.texture[10], GuildWarden.x[10], GuildWarden.y[10] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.BLACK_ORC ) )
	GuildWarden.texture[11], GuildWarden.x[11], GuildWarden.y[11] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.CHOPPA ) )
	GuildWarden.texture[12], GuildWarden.x[12], GuildWarden.y[12] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SQUIG_HERDER ) )

	GuildWarden.texture[13], GuildWarden.x[13], GuildWarden.y[13] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.ARCHMAGE ) )
	GuildWarden.texture[14], GuildWarden.x[14], GuildWarden.y[14] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SWORDMASTER ) )
	GuildWarden.texture[15], GuildWarden.x[15], GuildWarden.y[15] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SEER ) )
	GuildWarden.texture[16], GuildWarden.x[16], GuildWarden.y[16] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SHADOW_WARRIOR ) )

	GuildWarden.texture[17], GuildWarden.x[17], GuildWarden.y[17] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.WARRIOR_PRIEST ) )
	GuildWarden.texture[18], GuildWarden.x[18], GuildWarden.y[18] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.KNIGHT ) )
	GuildWarden.texture[19], GuildWarden.x[19], GuildWarden.y[19] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.WITCH_HUNTER ) )
	GuildWarden.texture[20], GuildWarden.x[20], GuildWarden.y[20] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.BRIGHT_WIZARD ) )

	GuildWarden.texture[21], GuildWarden.x[21], GuildWarden.y[21] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.RUNE_PRIEST ) )
	GuildWarden.texture[22], GuildWarden.x[22], GuildWarden.y[22] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.IRON_BREAKER ) )
	GuildWarden.texture[23], GuildWarden.x[23], GuildWarden.y[23] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.SLAYER ) )
	GuildWarden.texture[24], GuildWarden.x[24], GuildWarden.y[24] = GetIconData( Icons.GetCareerIconIDFromCareerLine( GameData.CareerLine.ENGINEER ) )

end