<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="GuildWarden" version="1.1.1" date="24/09/2009" >
        
        <Author name="Yxiomel" email="yxiomel@vitas-guild.org" />
        <Description text="Ward Guild Database" />
        
        <VersionSettings gameVersion="1.3.1" savedVariablesVersion="1.2" />

        <Dependencies>
	    <Dependency name="EA_ChatWindow"/>
        </Dependencies>
        
        <Files>
		<File name="icon.xml" />
		<File name="GuildWarden.xml" />
            <File name="GuildWarden.lua" />
            <File name="GuildWardenWin.lua" />
            <File name="GuildWardenTTips.lua" />
            <File name="classicons.lua" />
        </Files>

        <SavedVariables>
            <SavedVariable name="GuildWarden.Data" />
            <SavedVariable name="GuildWarden.light" />
            <SavedVariable name="GuildWarden.lastSendDate" />
            <SavedVariable name="GuildWarden.LvlU" />
            <SavedVariable name="GuildWarden.LvlL" />
            <SavedVariable name="GuildWarden.RrU" />
            <SavedVariable name="GuildWarden.RrL" />
            <SavedVariable name="GuildWarden.Combo1" />
            <SavedVariable name="GuildWarden.Combo2" />
            <SavedVariable name="GuildWarden.WardS" />


        </SavedVariables>
        
        <OnInitialize>
            <CallFunction name="GuildWarden.Init_Warden" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>
    </UiMod>
</ModuleFile>
